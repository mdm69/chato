<!DOCTYPE html>
<html lang="es">

<head>
    <title> Medicos</title>
    <meta charset="utf_8">
    <link rel="stylesheet" href="css/styles.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css" />

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
</head>

<body>

    <!--Cabecera-->
    <div class="container-fluid navbar-dark bg-info">
        <nav class="navbar navbar-expand-lg container">
            <a class="navbar-brand" href="index.php">Medicos</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Registro.php">Registro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Reportes.php">Medicos</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--Fin de la cabecera-->

    <!--DataTables-->
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead class="bg-info">
                            <tr>
                                <th>ID Medico</th>
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Fecha</th>
                                <th>Dni</th>
                                <th>Id Hospital</th>
                                <th>Servicio</th>
                                <th>Id paciente </th>
                                <th>Editar</th>
                                <th>Borrar</th>
                                
                            </tr>
                        </thead>
                        <tbody class="text-secondary">
                            <?php
                                //Conexion a la base de datos y sentencia de entrada de datos
                                $ConexionHD = new mysqli("localhost", "root", "", "hospital");
                                $sql = "SELECT * FROM medicos";
                                $query = mysqli_query($ConexionHD,$sql);
                                $array=mysqli_fetch_array($query);
                                do{
                                    echo "<tr>";
                                    echo "<td>".$array['id_medicos']."</td>";
                                    echo "<td>".$array['nombre']."</td>";
                                    echo "<td>".$array['ap_paterno']."</td>";
                                    echo "<td>".$array['ap_materno']."</td>";
                                    echo "<td>".$array['fecha_nacimiento']."</td>";
                                    echo "<td>".$array['dni']."</td>";
                                    echo "<td>".$array['id_hospital']."</td>";
                                    echo "<td>".$array['servicio']."</td>";
                                    echo "<td>".$array['id_pacientes']."</td>";
                                    echo "<td><spam>Editar</spam></td>";
                                    echo "<td><spam>Borrar</spam></td>";
                                    echo "</tr>";
                                }while($array = mysqli_fetch_array($query))
                                //Conexion con el servidor
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--Fin DataTables-->
    <!--Foother-->

    <!--Fin del foother-->
</body>

</html>

<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>