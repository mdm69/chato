<!DOCTYPE html>
<html lang="es">

<head>
    <title>MotorAlert</title>
    <meta charset="utf_8">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" href="icons/Alerta.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
</head>

<body>

    <!--Cabecera-->
    <div class="container-fluid navbar-dark bg-dark">
        <nav class="navbar navbar-expand-lg container">
            <a class="navbar-brand" href="index.php">MotorAlert</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Registro.php">Registro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Reportes.php">Reportes</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--Fin de la cabecera-->

    <!--Slider-->
    <div class="container-fluid fondo2">
        <div class="container d-flex flex-column justify-content-center h-100 align-items-center">

            <?php
        //Variables del formulario
        $Nombre=$_POST['nm_Nombre'];
        $ApellidoPaterno=$_POST['nm_ApPat'];
        $ApellidoMaterno=$_POST['nm_ApMat'];
        $Fecha=$_POST['dat'];
        $DNI=$_POST['dni'];
        $idhospital=$_POST['idhos'];
        $idservicio=$_POST['ids'];
        $idpasiente=$_POST['idpa'];

            $ConexionHD = new mysqli("localhost", "root", "", "hospital");
            $sql = "INSERT INTO medicos  VALUES (null,$DNI,'$Nombre','$ApellidoPaterno','$ApellidoMaterno','$Fecha',$idhospital,$idservicio,$idpasiente);";
            //Conexion con el servidor
            if (!$ConexionHD) {
                echo "<h1>No se a podido conectar con el servidor...</h1><p>Intentelo mas tarde</p>";
                header("Refresh:5; url=index.php");
            }else{
                if ($ConexionHD->query($sql) === TRUE) {
                    echo "<h1>Ingresando los datos...</h1><p>Espere por favor</p>";
                    header("Refresh:5; url=Reportes.php");
                } else {
                    echo "<h1>No se han podido subir los datos...</h1><p>intentelo mas tarde</p>";
                    header("Refresh:5; url=index.php");
                }
            }
            $ConexionHD->close();
          
        
        
      ?>
            <span class="spinner-border"></span>
        </div>
    </div>

</body>

</html>
