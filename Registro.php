<!DOCTYPE html>
<html lang="es">

<head>
    <title>Hospital</title>
    <meta charset="utf_8">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
</head>

<body class="bg-secondary">

    <!--Cabecera-->
    <div class="container-fluid navbar-dark bg-info">
        <nav class="navbar navbar-expand-lg container">
            <a class="navbar-brand" href="index.php">Añadir Medico</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Registro.php">Registro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Reportes.php">Medicos</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--Fin de la cabecera-->

    <!--Formulario-->
    <section class="container mt-5 bg-light FondoForm">
        <h1 class="text-upercase text-center pt-5 text-danger">Registro de Medicoss</h1>
        <div class="row p-5">
            <div class="col-12">
                <form action="GuardarReporte.php" method="post">
                    <!--Nombre-->
                    <div class="form-group row align-items-center">
                        <label for="" class="col-3 col-form-label">Nombre:</label>
                        <input class="form-control col-9" name="nm_Nombre" id="" type="text" placeholder="Nombre del medico" required pattern="[A-Za-zá-úÁ-Úñ]{5,30}" maxlength="30" autofocus>
                    </div>
                    <!--Apellido Paterno-->
                    <div class="form-group row align-items-center">
                        <label for="" class="col-3 col-form-label">Apellido Paterno:</label>
                        <input name="nm_ApPat" id="" type="text" placeholder="Apellido Paterno" class="form-control col-9" required pattern="[A-Za-zá-úÁ-Úñ]{5,30}" maxlength="30">
                    </div>
                    <!--Apellido Materno-->
                    <div class="form-group row align-items-center">
                        <label for="" class="col-3 col-form-label">Apellido Materno:</label>
                        <input name="nm_ApMat" id="" type="text" placeholder="Apellido Materno" class="form-control col-9" required pattern="[A-Za-zá-úÁ-Úñ]{5,30}" maxlength="30">
                    </div>
                    <!--Nombre Usuario-->
                    <div class="form-group row align-items-center">
                        <label for="" class="col-3 col-form-label">Fecha de Nacimiento:</label>
                        <input name="dat" id="" type="date" class="form-control col-9">
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="" class="col-3 col-form-label">DNI:</label>
                        <input name="dni" type="number"  placeholder="DNI" class="form-control col-9" required >
                    </div>

                    <div class="form-group row align-items-center">
                        <label class="col-3 col-form-label">Hispital:</label>
                        <select name="idhos" class="form-control col-9">
                            <option value="1">Hospital de los pequeñines</option>
                            <option value="2">Hospital de los super pequeñines</option>
                        </select>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-3 col-form-label">Sercicio:</label>
                        <select name="ids" class="form-control col-9">
                            <option value="1">Cirujano</option>
                            <option value="2">Cardiologo</option>
                        </select>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-3 col-form-label">Asignar a pasiente:</label>
                        <select name="idpa" class="form-control col-9">
                            <option value="1">Paola</option>
                            <option value="2">Abraham</option>
                        </select>
                    </div>                    

                    <div class="form-group align-items-center">
                        <input type="submit" class="btn btn-danger text-center" value="Enviar">
                    </div>
                </form>
            </div>
        </div>
    </section>

</body>

</html>
